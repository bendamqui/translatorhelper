<?php  
session_start();

require_once '../vendor/autoload.php';

if(file_exists('../app/core/config.php'))
{
	require_once '../app/core/config.php';
}
else
{
	die('Veuillez définir votre configuration à partir du fichier app/core/config.example.php. Remplacer ensuite le nom du fichier
		config.example.php  par config.php');
}

require_once '../app/core/functions.php';

require_once '../app/routes.php';


?>