<?php  
namespace App\Core;

use App\Core\View;

class Controller
{
	private $views_path= '../app/views/';

	public function __construct()
	{
		$this->view = new View;
	}
	public function loadViews($files, $data = [])
	{
		if(isset($_SESSION['errors']) AND $_SESSION['errors'])
		{
			$data['errors'] = $_SESSION['errors'];
			
			$_SESSION['errors'] = [];
		}
		if(isset($_SESSION['old']) AND $_SESSION['old'])
		{
			$data['old'] = $_SESSION['old'];

			$_SESSION['old'] = [];	
		}
		
		foreach ($files as $file) 
		{
			$path = $this->views_path.$file.'.php';
		
			if(file_exists($path))
			{
				require_once $path;
			}
			else
			{
				error_log('Le fichier view '.$path.' n\'existe pas');
			}
		}

	}
	public function redirect($page = '')
	{
		header('Location: '.URL.$page);
	}
}
?>