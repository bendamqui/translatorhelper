<?php  
namespace App\Core;

use App\Controllers;

class Router
{
	private $request_method;
	private $route;
	/**
	 * Listes des routes enregistrées
	 * @var array
	 */
	private $routes = [];
	
	public function __construct()
	{
		$this->setUri();
		$this->setRequestMethod();
	}
	private function setRequestMethod()
	{
		 $this->request_method = strtolower($_SERVER["REQUEST_METHOD"]);
	}
	/**
	 * Prépare l'uri à être comparé aux routes
	 */
	private function setUri()
	{
		$uri = (isset($_GET['uri'])) ? $_GET['uri'] : '/';

		if($uri != '/')
		{
			$uri = trim($uri,'/');
		}

		$this->uri = $uri;
	}
	/**
	 * Instancie le controller et appele la méthode associé à la route. Execute le
	 * closure si instane of = Closure
	 * Redirige vers 404 si l'uri ne match aucune route
	 */
	public function run()
	{		
		if($route = $this->match())
		{			
			$args = $this->getArgs($route['uri']);			
			
			if(is_string($route['callback']))
			{
				$part = explode('@', $route['callback']);				
								
				$controller = '\\App\\Controllers\\'.$part[0];
				
				$controller = new $controller;

				call_user_func_array([
					$controller, $part[1]
					], $args);
			}
			else
			{
				call_user_func($route['callback']);
			}
		}
		else
		{
			echo 'pas trouvé de route';
		}
	}
	/**
	 * Vérifie si des arguments sont passé dans l'uri et les retourne.
	 * @param  string $route_uri l'uri de la route enregistrée qui match ave l'uri 
	 * @return array            arguments contenu dans l'uri
	 */
	private function getArgs($route_uri)
	{
		$args = [];
		
		$route_uri = explode('/', $route_uri);
		
		$uri = explode('/', $this->uri);

		foreach($route_uri as $index => $value)
		{
			if(preg_match('/^\(/', $value))
			{
				$args[] = $uri[$index];
			}
		}		
		return $args;
	}
	/**
	 * Retoure la clé de la route qui match avec l'uri
	 * @return int 
	 */
	private function match()
	{		
		foreach($this->routes[$this->request_method] as $route)
		{
			if(preg_match('#^'.preg_replace('/(|)/', '', $route['uri']).'$#', $this->uri))			
			{
				return $route;				
			}
		}
		return false;
	}
	/**
	 * Affiches les routes enregistrées
	 * @return array
	 */
	public function getRoutes()
	{
		print_r($this->routes);
	}
	/**
	 * Ajoute une route à $this->routes
	 * @param string $uri      [uri]
	 * @param string|object   ['controller@method|closure']
	 */
	public function get($uri, $callback)
	{
		$this->routes['get'][] = [
			'uri' => $uri,
			'callback' => $callback
		];
	}
	public function post($uri, $callback)
	{
		$this->routes['post'][] = [
			'uri' => $uri,
			'callback' => $callback
		];
	}
}