<?php  
namespace App\Core;

class View
{
	private $view_folder = '../app/views/';

	public function render($files = [], $data = [])
	{
		if(isset($_SESSION['errors']) AND $_SESSION['errors'])
		{
			$data['errors'] = $_SESSION['errors'];
			
			$_SESSION['errors'] = [];
		}
		if(isset($_SESSION['old']) AND $_SESSION['old'])
		{
			$data['old'] = $_SESSION['old'];

			$_SESSION['old'] = [];	
		}
		
		foreach($files as $file)
		{
			$path = $this->view_folder.$file.'.php';
			if(file_exists($path))
			{
				require_once $path;
			}
		}
	}
}
?>