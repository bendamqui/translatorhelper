<?php  
/**
 * Chemin du dossier contenant les fichiers à traduire.
 * L'application assume que la structure des fichiers de language est la suivante:
 * lg/
 * 		fr/
 * 			files
 * 	  	en/
 * 		  	files
 */
define('LANG_FOLDER', "../app/example/");

//Url de l'application
define("URL", "http://translator.local/");

/**
 * Chemin de la langue de référence. L'application se base sur les fichiers contenu de ce doissier 
 * pour vérifier s'il y a du texte manquant dans les autres langues. 
 */
define("REF", "fr");

/**
 * Interrompre l'execution si configuration incomplète.
 */
if(URL === null)
{
	die("Éditez le fichier app/core/config afin de définir l'url de l'application");
}

if(LANG_FOLDER === null)
{
	die("Éditez le fichier app/core/config afin de définir le chemin du dossier contenant les fichiers à traduire");
}

?>