<div class="container">
	<div class="starter-template">
		<div class="row">
			<?php  
			if(isset($data['errors']) AND $data['errors'])
			{
				echo '<div class="col-xs-12">
						<p class="alert alert-danger">
							'.$data['errors'][0].'
						</p>
				</div>';
			}
			?>
			<div class="col-xs-12">
				<h2 class="page-header">Fichiers à traduire</h2>
			</div>			
		</div>
		<div class="row">
			<div class="col-xs-12">
			<ul class="list-group">
				<?php 
					foreach($data['files']['details'] as $file) 
					{
						echo '<li class="list-group-item">'.
								$file['lg'].'/'.$file['name'].
								'<span class="pull-right">
									<a href="'.URL.'files/'.$file['lg'].'/'.$file['name'].'">Voir</a> |								
									<a href="'.URL.'compare/'.$file['lg'].'/'.$file['name'].'">Comparer</a> |								
									<a href="'.URL.'translate/'.$file['lg'].'/'.$file['name'].'">Traduire</a>
								</span>
							</li>';						
					}
				?>
			</ul>					
			</div>
		</div>
	</div>
</div>