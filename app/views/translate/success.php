<div class="container">
	<div class="starter-template">
		<div class="col-xs-12">
			<p class="lead">Le tableau ci-dessous a été sauvegardé dans le fichier :</br> <?php  echo $data['path'] ?></p>
			<pre class="prettyprint">
<?php var_export($data['text']) ?>
			</pre>
		</div>
		<div class="col-xs-12">
			<a class="btn btn-primary" href="<?php echo URL.'files' ?>">Fichiers</a>
		</div>
	</div>
</div>
