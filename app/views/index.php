<div class="container">
	<div class="starter-template">
		<div class="row">
			
			<div class="col-xs-12">
				<h2 class="page-header">TranslatorHelper</h2>
			</div>			
			<div class="col-xs-12">
				<form action="<?php echo URL.'translate'; ?>" method="post"class="form-group">
				  <div class="form-group">
				    <label >Fichier source</label>
				    	<input type="text" class="form-control" name="_source" value="<?php echo isset($data['old']['_source']) ? $data['old']['_source'] : '' ?>">
				  </div>
				  <div class="form-group">
				    <label>Fichier cible</label>
				    	<input type="text" class="form-control" name="_target" value="<?php echo isset($data['old']['_target']) ? $data['old']['_target'] : '' ?>">
				  </div>	
				  <div class="form-group">
				  	<button type="submit" class="btn btn-default" name="_submit">Envoyer</button>
				  </div>				  
				</form>	
			</div>
			<div class="col-xs-12">
				<?php  
					if(isset($data['errors']) AND !empty($data['errors']))
					{
						foreach($data['errors'] as $error)
						{
							echo '<p class="alert alert-danger">'.$error.'</p>';
						}
					}
				?>
			</div>			
		</div>
	</div>
</div>