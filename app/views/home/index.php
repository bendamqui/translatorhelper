<div class="container">
	<div class="starter-template">
		<div class="row">
			<h1 class="page-header">TranslatorHelper</h1>	
			<p class="lead">
				TranslatorHelper facilite la gestion des fichiers de traductions pour les sites multilingues qui affiche le
				texte à l'aide d'arrays retournés depuis les fichiers de langues structurés de la façon suivante. 
			</p>
			<h5>Exemple : /app/lang/fr/menu.php</h5>
<pre class="prettyprint">
return [
	'home' => 'Accueil',
	'account' => 'Compte',
	'greetings' => [
		'hey' => 'ho!',
		'ho' => 'hey!'
	]
];
</pre>
			<p class="lead">
				L'application parcours les fichiers du dossier de la langue de référence définie dans app/core/config.php, compare
				le contenu de ces fichiers avec le contenu des fichiers des autres langues et renvoie la liste des fichiers dont la traduction est imcomplète. Si un fichier à traduire n'existe pas, bien qu'il apparaîtra dans la liste, vous devrez le créer manuellement.
			</p>
		</div>
	</div>
</div>

