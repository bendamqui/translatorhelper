<?php  
namespace App\Controllers;

use App\Core\Controller;
use App\Models\Text;

class FileController extends Controller
{
	function index()
	{		
		$model = new Text;

		$data['files'] = $model->getFilesToTranslate();				
		
		$data['badge'] = count($data['files']['names']);		
		
		$this->view->render([
			'layout/header',
			'files/index',
			'layout/footer'
		],
		$data);
	}
	public function show($lg, $file)
	{
		$model = new Text;

		$data['file'] = $model->getText($lg,$file);	

		$data['files'] = $model->getFilesToTranslate();		

		$data['badge'] = count($data['files']['names']);		

		$data['file_name'] = $lg.'/'.$file;
		
		$this->view->render([
			'layout/header',
			'files/file',
			'layout/footer'
		],
		$data);
	}
	public function compare($lg, $file)
	{
		$model = new Text;

		$data['text']['target'] = $model->getText($lg,$file);
		
		$data['text']['source'] = $model->getText(REF,$file);

		$data['file_name']['source'] = REF.'/'.$file;

		$data['file_name']['target'] = $lg.'/'.$file;

		$data['files'] = $model->getFilesToTranslate();		

		$data['badge'] = count($data['files']['names']);		

		$this->view->render([
			'layout/header',
			'files/compare',
			'layout/footer'
		],
		$data);

	}
}
?>