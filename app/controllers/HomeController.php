<?php  
namespace App\Controllers;

use App\Core\Controller;
use App\Models\Text;

class HomeController extends Controller
{
	function index()
	{		
		$model = new Text;

		$data['files'] = $model->getFilesToTranslate();	

		$data['badge'] = count($data['files']['names']);	

		$this->view->render([
			'layout/header',
			'home/index',
			'layout/footer'
		],
		$data);
	}
}
?>