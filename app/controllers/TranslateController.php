<?php  
namespace App\Controllers;

use App\Core\Controller;
use App\Lib\Validator;
use App\Lib\TranslatorHelper;
use App\Models\Text;

class TranslateController extends Controller
{
	public function index($lg,$file)
	{
		$input['source'] = LANG_FOLDER.REF.'/'.$file.'.php';
		
		$input['target'] = LANG_FOLDER.$lg.'/'.$file.'.php';

		$rules = [
			'source' => ['isFile', 'hasText'],
			'target' => ['isFile']
		];

		$validator = new Validator;

		if($validator->validate($rules, $input))
		{
			
			$translator = new TranslatorHelper;

			$text = $translator->getText($input['source'], $input['target']);				

			$data['missing_text'] = $translator->getMissingText($text['source'], $text['target']);
			
			$data['file_name'] = $file;

			$model = new Text;

			$data['files'] = $model->getFilesToTranslate();		

			$data['badge'] = count($data['files']['names']);		

			$this->view->render([
					'layout/header',
					'translate/index',
					'layout/footer'
			], $data);			
		}
		else
		{
			$this->redirect('files');
		}

	}
	public function save($lg,$file)
	{
		$input['source'] = LANG_FOLDER.REF.'/'.$file.'.php';
		
		$input['target'] = LANG_FOLDER.$lg.'/'.$file.'.php';

		$rules = [
			'source' => ['isFile', 'hasText'],
			'target' => ['isFile']
		];

		$validator = new Validator;

		if($validator->validate($rules, $input))
		{			
			$translator = new TranslatorHelper();

			$input_text = $translator->sortPost($_POST);						

			$text = $translator->getText($input['source'], $input['target']);						
			
			$new_text = $translator->fillArray($input_text, $text['target']);

			$translator->save($input['target'], $new_text);	

			$data['text'] = $new_text;
			
			$data['path'] = $input['target'];

			$model = new Text;

			$data['files'] = $model->getFilesToTranslate();		

			$data['badge'] = count($data['files']['names']);		

			$this->view->render([
				'layout/header',
				'translate/success',
				'layout/footer'], 
				$data);					

		}
		else
		{			
			$this->redirect('files');
		}
	}
}
?>