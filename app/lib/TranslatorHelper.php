<?php  
namespace App\Lib;
class TranslatorHelper
{		
	
	public function getText($source, $target)
	{				
		$text['source'] = require($source);			
		
		$text['target'] = require_once($target);					

		$text['target'] = (is_array($text['target'])) ? $text['target'] : [];
				
		return $text;
	}
	/**
	 * Compare le fichier à traduire au fichier de référence (fr) retourne un tableau[0]['key', 'text'] des éléments 
	 * non traduits	
	 * @param  array $source tableau de référence	
	 * @param  array $target tableau à vérifier
	 * @return array  tableau[0]['key' => 'str-key', 'text' => 'texte de source']
	 */
	public function getMissingText($source, $target, $strK = '', $res = [])
	{
		foreach($source as $k => $v)
		{
			if(is_array($v))
			{
				$strToPass = $strK.'-'.$k;
				$res = $this->getMissingText($v, $target, $strToPass, $res);
			}
			else
			{
				$str = $strK.'-'.$k;
				if(!$this->keyExists($str, $target))
				{					
					$res[] = ['key' => $str, 'txt' => $v];
				}
			}
		}			
		return $res;		
	}
	public function keyExists($strK, $target)
	{
		$keys = explode('-', trim($strK,'-')); 

		foreach($keys as $key)
		{
			if(isset($target[$key]) AND !empty($target[$key]))
			{
				$target = $target[$key];
			}
			else
			{
				return false;
			}
		}
		return true;
	}
	public function fillArray($post, $target)
	{
		foreach ($post as $key => $value) 
		{
			$parts = explode('-', $key);

			$tmp =& $target;
			
			for($i = 0; $i < count($parts); $i++) 
			{
				if($i == count($parts) -1)
				{
					$tmp[$parts[$i]] = $value;
				}
				else
				{
					$tmp =& $tmp[$parts[$i]];
				}
			}
		}
		return $target;
	}
	public function _fillArray($post, $target)
	{
		foreach($post as $key => $value)
		{
			$parts = explode('-', $key);

			$count = count($parts);

			switch ($count) 
			{
    			case 1:
        			$target[$parts[0]] = $value;
        			break;
        		case 2:
        			$target[$parts[0]][$parts[1]] = $value;
        			break;
        		case 3:
        			$target[$parts[0]][$parts[1]][$parts[2]] = $value;
        			break;
        		case 4:
        			$target[$parts[0]][$parts[1]][$parts[2]][$parts[3]] = $value;
        			break;
        		case 5:
        			$target[$parts[0]][$parts[1]][$parts[2]][$parts[3]][$parts[4]] = $value;
        			break;
			}
		}
		return $target;
	}
	public function sortPost($post)
	{
		$res = [];
		foreach ($_POST as $k => $v) 
		{			
			if($k{0} != '_' AND !empty($v))
			{
				$res[trim($k, '-')] = $v; 
			}
		}
		return $res;
	}
	public function save($target_path, $text)
	{							
		if(file_put_contents($target_path, "<?php \n return " . var_export($text, true) . ";"))
		{			
			return true;
		}
		else
		{
			return false;
		}										
	}
}

?>