<?php  
namespace App\Lib;
class Validator
{		
	private $errors = [];	

	public $error_messages = [];

	private $errors_map = [
		'source' => [
			'isFile' => 'L\'url de référence  n\'existe pas ou ne contient pas de texte.',
			'hasText' => 'Le fichier de référence ne contient pas de texte.'
		],
		'target' => [			
			'isFile' => 'Le fichier cible n\'existe pas. Veuillez le créer manuellement avant de procéder à la traduction.',
		]
	];
	
	public function getErrors()
	{
		return $this->errors;
	}
	public function validate($rules, $input)
	{
		foreach ($rules as $field => $rule) 
		{	
			foreach ($rule as $method) 
			{				
				$method_name = 'validate'.ucfirst($method);

				if(!$this->$method_name($input[$field]))
				{
					$this->errors[] = [
						'field' => $field,
						'rule' => $method,
						'input' => $input[$field]
					];
				}				
			}			
		}
		if($this->errors)
		{
			$this->setErrorMessages();			
			return false;
		}
		else
		{
			return true;
		}				
	}
	private function setErrorMessages()
	{
		foreach ($this->errors as $error) 
		{
			$_SESSION['errors'][] = $this->errors_map[$error['field']][$error['rule']];
		}
	}
	private function validateRequired($input)
	{
		if(!isset($input) OR empty($input))
		{			
			return false;
		}		
		return true;
	}
	private function validateIsFile($input)
	{
		if(!file_exists($input))
		{
			return false;
		}
		return true;
	}
	private function validateHasText($input)
	{
		if(file_exists($input))
		{
			$text = require($input);			
			
			if(is_array($text))
			{
				return true;
			}
			return false;
		}
		return false;
	}
}
?>