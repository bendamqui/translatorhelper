<?php  
namespace App\Models;

use App\Core\Model;
/**
* Home
*/
class Text extends Model
{
	private $ref_lg;

	private $lang_folder;
	
	public function __construct()
	{
		$this->ref_lg = REF;
		
		$this->lang_folder = LANG_FOLDER;
	}
	public function scanLangFolder()
	{
		$res = [];
		$folders = scandir($this->lang_folder);	
		foreach ($folders as $lg) 	
		{			
			if(preg_match('/[a-z]{2}/', $lg))
			{																								
				$files = scandir($this->lang_folder.$lg);					

				foreach ($files as $file) 
				{
					if(preg_match('/\.php$/', $file))
					{												
						$text = require ($this->lang_folder.$lg.'/'.$file);
						$res[$lg][preg_replace('/\.php$/', '', $file)] = (is_array($text)) ? $text : [];						
					}
				}				
				if(!isset($res[$lg]))
				{
					$res[$lg] = [];
				}
			}
		}
		return $res;
	}
	public function getText($lg,$file)
	{
		$path = $this->lang_folder.$lg.'/'.$file.'.php';
		
		if(file_exists($path))
		{
			$text = require($path); 

			return(is_array($text)) ? $text: [];
		}
		else
		{
			return [];
		}
	}
	public function getFilesToTranslate()
	{		
		$text = $this->scanLangFolder();
		
		$lgs = $this->getLgs();				

		$res = $this->compareArrays($text,$lgs);	

		return $res;
		
	}
	private function getLgs()
	{
		$lgs = [];

		$folders = scandir($this->lang_folder);	
		
		foreach ($folders as $lg) 
		{
			if(preg_match('/[a-z]{2}/', $lg) AND $lg != $this->ref_lg)
			{
				$lgs[] = $lg;
			}			
		}
		return $lgs;
	}
	private function compareArrays($text,$lgs)
	{
		$res = [];
		foreach($text[$this->ref_lg] as $file => $ref_text) 
		{
			foreach ($lgs as $lg) 
			{
				if(!isset($text[$lg][$file]) OR $this->hasMissingKeys($text[$lg][$file], $this->getKeys($ref_text)))
				{					
					$res['names'][] = $lg.'/'.$file;
					$res['details'][] = [
						'lg' => $lg,
						'name' => $file
					];				
				}
			}
		}
		return $res;
	}	
	private function hasMissingKeys($text, $keys)
	{										
		foreach ($keys as $key) 
		{			
			$parts = explode('-', $key);									
			
			$tmp = $text;
			foreach ($parts as $part) 
			{				
				
				if(isset($tmp[$part]) AND !empty($tmp[$part]))
				{
					$tmp = $tmp[$part];				
				}
				else
				{					
					return true;
				}
			}
		}
		return false;
	}
	private function getKeys($text, $key = '', $res = [])
	{
		foreach ($text as $k => $v) 
		{												
			
			if(is_array($v))
			{
				$keyToPass = $key.'-'.$k;				
				$res = $this->getKeys($v, $keyToPass,$res);
			}
			else
			{
				$res[] = trim($key.'-'.$k,'-');
			}
		}		
		return $res;
	}
}
?>