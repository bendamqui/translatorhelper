<?php  

use App\Core\Router;

$router = new Router;

$router->get('/','HomeController@index');

$router->get('files', 'FileController@index');

$router->get('files/([a-z]{2})/([a-zA-Z0-9\-_]+)', 'FileController@show');

$router->get('compare/([a-z]{2})/([a-zA-Z0-9\-_]+)', 'FileController@compare');

$router->get('translate/([a-z]{2})/([a-zA-Z0-9\-_]+)', 'TranslateController@index');

$router->post('translate/([a-z]{2})/([a-zA-Z0-9\-_]+)', 'TranslateController@save');

$router->run();


?>